% works in matlab 2019b
% �ukasz Giemza


clear
r = 0.002; % radius of the wire [m]
R = 2 * 0.0254;  % radius od the coil [m]
m = 0.01; %mass [kg]
g = 9.80665; % gravitational acceleration (m/s^2)
G = 80E9; %structural steel rigidity modulus [Pa]
tau = 2; % the time suppression constant [s]
beta = 1 / 2 * tau ; %damping factor
omega = 50:0.5:400; %frequency [1/s]
alpha_0 = g;
F_0 = m * g;
N = 2:50; % list of the number of coils in the spring

fig = figure('Name','Graph','NumberTitle','off'); % creating figure window

subplot(2,1,1); %creating axes in tiled positions
color = ['r','b','g','y','m','ky']; %color list for plots

file_ID = fopen('resonance_analysis_results.dat', 'w'); %creating new file for writing
fprintf(file_ID,'G = %.2d [Pa], r = %.0d [m], R = %.3f [m], m = %.2f [kg], g = alpha = %.2f [m/s^2], tau = %d [s] \n\n', ...
 G, r, R, m, g ,tau); %writing constant values to file

hold on; %saving current charts when adding new ones,  this part taken from Micha� Koruszowic

%calculating  amplitude depending on the number of coils for different frequencies
for i=1:size(N,2)
  for t=1:size(omega,2)
      k = G * r^4/(4 * N(i) * R^3);   
      omega_0 = sqrt(k / m);
      y(t) = alpha_0 / sqrt((omega_0^2 - omega(t)^2)^2 + 4 * beta^2 * omega(t)^2); 
  end
  plot(omega / (2*pi), y, color(mod(i,6)+1)); %creating a plot
end
hold off;

%setting title and labels for X and Y axis
title('A(f) for diffrent N'); 
ylabel('Amplitude of forced vibration A, m');
xlabel('Frequency f, Hz');

subplot(2,1,2); 
%calculating resonance amplitude depending on the number of coils
for p=1:size(N,2)
  k = G * r^4/(4 * N(p) * R^3);
  omega_0 = sqrt(k / m);
  A(p) = alpha_0 / sqrt((4 * beta^2 * omega_0^2));
  fprintf(file_ID,'N = %d, A(N) = %.3f [m], f = %.3f [Hz] \n', N(p), A(p), omega_0); %writing data to file
end

fclose(file_ID); 

%setting title and labels for X and Y axis
plot(N,A);
title('A(N)');
xlabel('Frequency [Hz]');
ylabel('Amplitude  [m]');        



saveas(fig, 'resonance.pdf'); % saving figure to pdf format




